const express = require('express');

const protect = require('../middleware/protect');
const auth = require('../controllers/auth');

const router = express.Router();

router.post('/signup', auth.signUp);
router.post('/signin', auth.signIn);
router.post('/forgot-password', auth.forgotPassword);
router.patch('/reset-password/:key', auth.resetPassword);
router.patch('/change-password', protect, auth.changePassword);
router.post('/logout', protect, auth.logout);

module.exports = router;
