const express = require('express');
const user = require('../controllers/user');

const router = express.Router();

router.get('/', user.getUser);
router.post('/', user.update);

module.exports = router;
