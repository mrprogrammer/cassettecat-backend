const app = require('./app');

const PORT = process.env.PORT || 3001;

app.listen(PORT, err => {
  if (err) {
    console.log('Error To Starting App :', err);
  }
  console.log(`App Runing On Port ${PORT} ...`);
});
