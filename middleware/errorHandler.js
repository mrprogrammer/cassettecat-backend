module.exports = (err, req, res, next) => {
  if (process.env.NODE_ENV === 'development') {
    console.log(err);
  }
  return res.status(err.statusCode).json({
    message: err.message
  });
};
