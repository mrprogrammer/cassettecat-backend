const crypto = require('crypto');
const User = require('../models/user');
const Err = require('../utilities/Err');

exports.signUp = async (req, res, next) => {
  try {
    const user = await User.create(req.body);
    if (!user) {
      throw 'user not created!';
    }
    const token = await user.generateTokenAndSave();
    res.status(201).json({
      data: { user },
      token
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.signIn = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    if (!email || !password) {
      throw 'Email & Password is Required!';
    }
    const user = await User.findByCredential(email, password);
    const token = await user.generateTokenAndSave();
    res.json({
      data: { user },
      token
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.forgotPassword = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      throw 'this email is not exit';
    }
    const securityKey = user.generateSecurityKey();
    await user.save({ validateBeforeSave: false });
    console.log('securityKey', securityKey);
    // Submit securityKey To User Email Or Mobile Number
    res.json({
      data: { msg: 'OK!' }
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.resetPassword = async (req, res, next) => {
  try {
    const criptoResetKey = crypto
      .createHash('sha256')
      .update(req.params.key)
      .digest('hex');
    const user = await User.findOne({
      securityKey: criptoResetKey,
      securityKeyExpireAt: { $gt: Date.now() }
    });
    if (!user) {
      throw 'key not accepted';
    }
    user.token = [];
    user.password = req.body.password;
    user.securityKey = undefined;
    user.securityKeyExpireAt = undefined;
    const token = await user.generateTokenAndSave();
    res.json({
      data: { user },
      token
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.changePassword = async (req, res, next) => {
  try {
    const { password, newPassword } = req.body;
    const { user } = req;
    const passwordMatch = await User.passwordMatch(password, user.password);
    if (!passwordMatch) {
      throw 'password is wrong!';
    }
    user.tokens = user.tokens.filter(token => {
      return token.token !== req.token;
    });
    user.password = newPassword;
    const token = await user.generateTokenAndSave();
    res.json({
      data: { user },
      token
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.logout = async (req, res, next) => {
  try {
    req.user.tokens = req.user.tokens.filter(token => {
      return token.token !== req.token;
    });
    await req.user.save();
    res.json({
      data: { message: 'bye bye ...' }
    });
  } catch (error) {
    next(new Err(error, 500));
  }
};
