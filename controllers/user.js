const User = require('../models/user');
const Err = require('../utilities/Err');

exports.getUser = async (req, res, next) => {
  try {
    res.json({
      data: req.user
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};

exports.update = async (req, res, next) => {
  try {
    const data = { ...req.body };
    User.denyItems().forEach(item => {
      delete data[item];
    });
    const user = await User.findByIdAndUpdate(req.user.id, data, {
      new: true,
      runValidators: true
    });
    res.json({
      data: user
    });
  } catch (error) {
    next(new Err(error, 400));
  }
};
