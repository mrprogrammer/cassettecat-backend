const express = require('express');
const mongoose = require('mongoose');

const Err = require('./utilities/Err');
const errorHandler = require('./middleware/errorHandler');
const protect = require('./middleware/protect');

const auth = require('./routes/auth');
const user = require('./routes/user');

const MONGO_DB_URL = `mongodb://${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}/${process.env.MONGO_DB_NAME}`;
mongoose
  .connect(MONGO_DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log('MongoDB Connected !'))
  .catch(err => console.log('MongoDB Exeption to connect : ', err));

const app = express();

app.use(express.json({ limit: '100kb' }));
app.use(express.urlencoded({ extended: true, limit: '100kb' }));

app.use('/', auth);
app.use('/user', protect, user);

app.all('*', (req, res, next) => {
  next(new Err(`ROUTE NOT FOUND!`, 404));
});

app.use(errorHandler);

module.exports = app;
