const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const schema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Name Is Required']
    },
    email: {
      type: String,
      required: [true, 'Email Is Required'],
      unique: true,
      trim: true,
      lowercase: true,
      validate: [validator.isEmail, 'Email Is Not Valid']
    },
    avatar: {
      type: String,
      default: 'avatar.jpg'
    },
    active: {
      type: Boolean,
      default: true,
      select: false
    },
    password: {
      type: String,
      required: [true, 'Password Is Required'],
      minlength: 8
    },
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ],
    securityKey: String, // This Item For Check Email Or Mobile Number Validation. This Filed Generate After Register Or Forgot Password
    securityKeyExpireAt: Date
  },
  {
    timestamps: true
  }
);

// Password Hash & Clean Confirm Password
schema.pre('save', async function(next) {
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 8);
  }
  next();
});

// Just Get Active Users
schema.pre(/^find/, function(next) {
  this.find({ active: { $ne: false } });
  next();
});

// Generate Token And Save User
schema.methods.generateTokenAndSave = async function() {
  const user = this;
  const token = jwt.sign(
    { _id: user._id.toString() },
    process.env.JWT_SECRET_KEY
  );
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
};

schema.statics.findByCredential = async (email, password) => {
  if (!email || !password) {
    throw 'email and password is required!';
  }
  const user = await Model.findOne({ email });
  if (!user) {
    throw 'Unable to login';
  }
  const isMatch = await Model.passwordMatch(password, user.password);
  if (!isMatch) {
    throw 'Unable to login';
  }
  return user;
};

// Compare Password In Sing In Route
schema.statics.passwordMatch = async function(password, userPassword) {
  return await bcrypt.compare(password, userPassword);
};

// Make Reset Key To Reset Password
schema.methods.generateSecurityKey = function() {
  const resetKey = crypto.randomBytes(20).toString('hex');
  this.securityKey = crypto
    .createHash('sha256')
    .update(resetKey)
    .digest('hex');
  this.securityKeyExpireAt = Date.now() + 20 * 60 * 1000;
  return resetKey;
};

const hiddenItems = [
  '__v',
  'active',
  'password',
  'tokens',
  'securityKey',
  'securityKeyExpireAt'
];

schema.methods.toJSON = function() {
  const user = this;
  const userObj = user.toObject();
  hiddenItems.forEach(item => {
    delete userObj[item];
  });
  return userObj;
};

schema.statics.denyItems = () => [
  ...hiddenItems,
  'id',
  '_id',
  'email',
  'avatar'
];

const name = 'User';
schema.statics.getName = () => name;
const Model = mongoose.model(name, schema);

module.exports = Model;
